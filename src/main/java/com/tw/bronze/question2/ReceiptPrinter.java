package com.tw.bronze.question2;

import java.util.HashMap;
import java.util.List;

public class ReceiptPrinter {
    private final HashMap<String, Product> products =
        new HashMap<>();

    public ReceiptPrinter(List<Product> products) {
        // TODO:
        //   Please implement the constructor
        // <-start-
        for (Product product : products) this.products.put(product.getId(), product);
        // --end->
    }

    public int getTotalPrice(List<String> selectedIds) {
        // TODO:
        //   Please implement the method
        // <-start-
        int totalPrice = 0;
        if (null == selectedIds) {
            throw new IllegalArgumentException();
        } else if (selectedIds.size() == 0) {
            return totalPrice;
        } else {
            String[] selectedId = (String[])selectedIds.toArray();
            for (int i = 0; i < selectedId.length; i++) {
                for (String key : products.keySet()) {
                    if (key.equals(selectedId[i])) {
                        Product selectedProducts = products.get(key);
                        int selectedPrices = selectedProducts.getPrice();
                        totalPrice += selectedPrices;
                    }
                }
            }
            return totalPrice;
        }
        // --end->
    }
}

