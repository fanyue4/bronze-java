package com.tw.bronze.question1;

import java.util.HashSet;

public class CollectionOperations {
    /**
     * This method will remove all the duplicated `String` values from a
     * collection.
     *
     * @param values The collection which may contain duplicated values
     * @return A new array removing the duplicated values.
     */
    public static String[] distinct(Iterable<String> values) {
        // TODO:
        //  Please implement the method. If you have no idea what Iterable<T>
        //  is, please refer to the unit test, or you can have a search on
        //  the internet, or you can find it in our material.
        // <-start-
        if (values == null) {
            throw new IllegalArgumentException();
        }

        HashSet<String> duplicatedList = new HashSet<>();

        for (String value : values) {
            duplicatedList.add(value);
        }

        Object[] nonDuplicatedList = duplicatedList.toArray();
        String result[] = new String[nonDuplicatedList.length];

        for (int i = 0; i < nonDuplicatedList.length; i++) {
            result[i] = (String) nonDuplicatedList[nonDuplicatedList.length - 1 - i];
        }
        return result;

        // --end->
    }
}
